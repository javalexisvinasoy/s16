/*console.log("Hello");*/

// Arithmetic Operators

/*
	+ sum
	- subtraction
	* multiplication
	/ division
	% modulo (return of remainder)


*/

let x = 45;
let y = 28;

let sum = x + y;
console.log("Result of addition: " + sum);

let difference = x - y;
console.log("Result of subtraction: " + difference);

let product = x * y;
console.log("Result of multiplication: " + product);

let quotient = x / y;
console.log("Result of division: " + quotient);

let mod = x% y;
console.log("Result of Modulo: " + mod);

// Assignment Operator
// Basic assignment operator (=)

let assignmentNumber = 8;

// Arithmetic Assignment Operator
// Addition Assignment Operator (+=)
// Subtraction Assignment Operator (-=)
// Multiplication Assignment Operator (*=)
// Division Assignment Operator (/=)
// Modulo Assignment Operator (%=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("")

// PEMDAS
/*
	P - Parenthesis
	E - Exponent
	M - Multiplication
	D - Division
	A - Addition
	S - Subtraction
*/

let  mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS: " + mdas);

let pemdas = 1 + (2 - 3) * (4 - 5);
console.log("Result of PEMDAS: " + pemdas);

// Increment and Decrement
/* This are the Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.*/
let z = 1;

let increment = ++z; // z = z + 1
// The value "z" is added by a value f one before returning the value and storing it in variable "increment"
console.log("Pre-increment: " + increment);
// result: increment - 2
// The value of "z" was also increased even though we did not implicity specify any value reassignment
console.log("Value of Z: " + z);
// result: z - 2

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
// increment = 2 plus 1 (z + 1)
console.log("Post-increment: " + increment);
// The value of "z" was increased again reassigning the value to 3
console.log("Value of Z: " + z);

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"
let decrement = --z;
// The value of "z" is at before it was decremented. Result :2
console.log("Pre-decrement: " + decrement);
console.log("Value of Z: " + z);

// The value of "z" is returned and stored in the variable "decrement" then the value of z is decreased by 1
decrement = z--;
// The value of z at 2 before it was decremented
console.log("Post-decrement: " + decrement);
// The value of "z" was decreased reassigning the value of 1
console.log("Value of Z: " + z); 



// Type of Coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
	 - this happens when operation are performed on different date types that would normally not be possible and yield irregular results
	 - to address the issue.. values are automatically converted from one data type to another in order to resolved operations
	
*/
let numA = '10';
let numB = 12;

/*
	Adding or concatenating a string and a number will result in a string
*/
let coercion = numA + numB;
console.log(coercion); // result: 1012 - Concatenation
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);

let numE = true + 1;
/*
	The boolean "true" is also associated with the value of 1
	boolean "fales" is associated with value of 0
*/
console.log(numE); // result: 1 + 1 = 2

let numF = false + 2;
console.log(numF);  // result: 0 + 2 = 2

let numG = true + 5;
console.log(numG);




// Equality Operator (==)
/*
	- Checks whether the operance are equal/have the same content 
	- attempts to CONVERT and COMPARE operance of different data types
*/
console.log(1 == 1); // result: true, 1 is equals to 1
console.log(1 == 2); // result: false , 1 is not equals to 2
console.log(1 == "1"); // result: true, because same content
console.log('jungkook' == "jungkook"); // result: true, because same content
console.log('Jungkook' == "jungkook"); // result: false
console.log("a" == "A"); // result: false, (61 == 41) - aski code value


// Strict Equality (===)
/*
	- Checks whether the operance are equal or have the same content
	- ALSO COMPARES the date type of 2 values
	- Javascript is a loosely typed language - meaning that values of differendate types can be stored in variables
		- in combination with type coercion, this sometimes creates problems within our code (e.g Java, Typescript)
	- Strict equality operators are better to use in most cases to ensure that data types provided are correct	
*/
console.log (1 === 1); // result: true
console.log (1 === "1"); // result: false 

let johnny = 'johnny' ;
console.log('johnny' === johnny); // result: true

console.log(false === 0); //result: false


// Inequality Operator (!=) - is not equal to
/*
	- Checks whether the operands are not equal/ have different content
	- attempts the CONVERT and COMPARE operands of different date type
*/
console.log(1 != 1); // result: false
console.log(1 != 3); // result: true
console.log(1 != "1") // result: false
console.log('jungkook' != "jungkook"); // result: false
console.log('Jungkook' != "jungkook"); // result: true

// Strict Inequality (!==)
/*
	- Checks whether the operance are not equal or have the same content
	- also COMPARES the date type of 2 values
*/

console.log(1 !== 1); //result: false
console.log(1 !== "1"); // result: true

// Relational Operator
// Logical Operator